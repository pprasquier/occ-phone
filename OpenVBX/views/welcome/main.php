<div id="welcome-container">
	<h1 id="openvbx-logo"><a href="<?php echo site_url() ?>/"><span class="replace">On-call Phone</span></a></h1>

	<form id="install-form" method="post" action="">
		<div id="welcome-steps">
			<div class="error ui-widget-overlay"><?php if(isset($error)) echo $error; ?></div>

			<div class="steps">
				
				<div class="step next">
					<a target="_blank" class="help" href="http://openvbx.org/install#upgrade" title="Get help at OpenVBX.org">Help</a>
					
					<?php if (!empty($tenant_sid) && $tenant_sid == 'unauthorized_client'): ?>
						<h1>Connect Access Denied</h1>
					<?php else: ?>
						<h1>Connect</h1>
					<?php endif; ?>
					
					<div class="step-desc">
						
						<?php if (!empty($tenant_sid) && $tenant_sid == 'unauthorized_client'): ?>
							<div class="upgrade-error .error">
								<p>You are required to authorize On-call CIO to access your account to use On-call Phone.</p> 
							</div>
						<?php else: ?>
							<br />
							<p>You need to connect your On-call Phone Account.</p>
						<?php endif; ?>

						<br />
						<p>Click &ldquo;Continue&rdquo; to authorize On-call Phone to access your account.</p>
						
						<div class="upgrade-warning">
							<p>If you do not have an Account you can set up one during the next step.</p>
						</div>
						
					</div><!-- .step-desc -->
				</div><!-- .step -->

				<div class="step submit">
					<h1>Connect Complete</h1>
					<div class="step-desc">
						<p>Your install is now ready!</p>
					</div><!-- .step-desc -->
				</div><!-- .step -->
				
			</div><!-- .steps -->

	<?php if (!isset($error)): ?>
			<div class="navigation">
				<button class="prev">&laquo; Previous</button>
				<button class="next">Continue &raquo;</button>
				<button class="submit">Continue to Inbox &raquo;</button>
			</div>
	<?php endif; ?>

		</div><!-- #welcome-steps -->
	</form>
</div><!-- #install-container -->